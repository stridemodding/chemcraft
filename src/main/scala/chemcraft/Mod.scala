package ChemCraft

import cpw.mods.fml.common.{SidedProxy, Mod}
import chemcraft.core.proxy.CommonProxy
import cpw.mods.fml.common.event.{FMLPostInitializationEvent, FMLInitializationEvent, FMLPreInitializationEvent}

/**
 * Mod
 * ChemCraft
 * License: GPL v3
 **/
@Mod(modid = Constants.Modid, name = Constants.Modname, version = Constants.Version, modLanguage = "scala")
object ChemCraft {

  @SidedProxy(clientSide = Constants.ClientProxy, serverSide = Constants.CommonProxy)
  var proxy: CommonProxy = null

  def preInit(event: FMLPreInitializationEvent){

  }

  def init(event: FMLInitializationEvent){

  }

  def postInit(event: FMLPostInitializationEvent){

  }
}

object Constants{

  final val Modid = "chemcraft"
  final val Modname = "Chem Craft"
  final val Version = "0"
  final val clientSide = "chemcraft.core.proxy.ClientProxy"
  final val serverSide = "chemcraft.core.proxy.CommonProxy"
}